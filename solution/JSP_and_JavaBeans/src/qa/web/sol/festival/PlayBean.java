package qa.web.sol.festival;

import java.io.Serializable;

/**
 * @author Lewis
 *
 */
public class PlayBean implements Serializable {
	
	private String play;
	private String author;
	private String time;
	private int day;
	private boolean resultsObtained;
	
	/**
	 * Constructor for PlayBean.
	 */
	public PlayBean() {
	}

	/**
	 * The setDay() method which assigns results.
	 */
	public void setDay(int day) {	
		this.day = day;
		if (day < 1 || day > plays.length) {
			resultsObtained = false;
			play = null;
			author = null;
			time = null;
		} else {
			resultsObtained = true;
			play = plays[day-1][1];
			author = plays[day-1][2];
			time = plays[day-1][3];			
		}
	}
	
	public boolean getResultsObtained() {return resultsObtained;}
	public String getPlay() {return play;}
	public String getAuthor() {return author;}
	public String getTime() {return time;}
	public int getDay() {return day;}
	
	private static String[][] plays = {
		{"1201","Hamlet","Shakespeare","7:00pm","1"},
		{"1202","Pygmallion","Bernard Shaw","8:00pm","2"},
		{"1203","Salome","Wilde","7:30pm","3"},
		{"1204","Amadeus","Schaffer","7:30pm","4"},
		{"1205","Waiting for Godot","Beckett","8:00pm","5"},
		{"1206","Death of a Salesman","Miller","8:00pm","6"},
		{"1207","Much Ado About Nothing","Shakespeare","7:00pm","7"},
		{"1208","Doctor Faustus","Marlowe","8:00pm","8"},
		{"1209","Richard III","Shakespeare","7:00pm","9"},
		{"1210","Woyzeck","Buchner","8:00pm","10"},
		{"1211","Brimstone and Treacle","Potter","7:30pm","11"},
		{"1212","Democracy","Frayn","7:30pm","12"},
		{"1213","The Frogs","Aristophanes","8:00pm","13"},
		{"1214","Oedipus Rex","Sophocles","8:00pm","14"}
	};

	

}
