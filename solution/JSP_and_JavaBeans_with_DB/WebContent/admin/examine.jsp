<%@ page import='java.sql.*,javax.sql.*,javax.naming.*' %>

<html>
<body>
<h1>Table Examination</h1>
<hr>

<%!
	// Specifics
	private String jndi = "java:comp/env/jdbc/festival";
	private String[] tablenames = {"plays", "films", "operas", "events"};
%>

<%
try {
	InitialContext ic = new InitialContext();
	DataSource ds = (DataSource) ic.lookup(jndi);
	Connection con = ds.getConnection();
	Statement stmt = con.createStatement();
	for (int x=0; x<tablenames.length; x++) {
		String sql = "SELECT * FROM DBO." + tablenames[x];
		out.println("<br><font color='blue'>" + sql + "</font>");
		try {
			ResultSet res = stmt.executeQuery(sql);
			ResultSetMetaData md = res.getMetaData();
			out.println("<div align='center'>");
			out.println("<table border='1' cellpadding='2'>");
			out.println("<tr>");
			for (int i=0; i<md.getColumnCount(); i++) {
				out.println("<th>" + md.getColumnName(i+1) + "</th>");
			}
			out.println("</tr>");
			while (res.next()) {
				out.println("<tr>");
				for (int i=0; i<md.getColumnCount(); i++) {
					out.println("<td>" + res.getString(i+1) + "</td>");
				}
				out.println("</tr>");
			}
			out.println("</table>");
			out.println("</div>");
			res.close();
		} catch (Exception e) {
			out.println("<br><font color='red'>" + e.getMessage() + "</font>");
		}
	}
	out.println("<br><font color='blue'>Closing Connection.</font>");
	con.close();
} catch (Exception e) {
	e.printStackTrace(System.err);
}
%>

</body>
</html>