package qa.shop;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class CartTest {
	
	private Cart cart;

	@Before
	public void init() throws Exception {
		cart = new Cart();
	}
	
	@Test
	public void testTwoForThePriceOfOne() {
		cart.addItem(new ShopItem("Teddy Bear", 23.0));
		cart.addItem(new ShopItem("Teddy Bear", 23.0));
		assertEquals(cart.getPrice(), 23.0, 0.001);
	}

	@Test
	public void testPriceOfDistinct() {
		cart.addItem(new ShopItem("Teddy Bear", 23.0));
		cart.addItem(new ShopItem("Train Set", 49.99));
		assertEquals(cart.getPrice(), 72.99, 0.001);
	}

	@Test
	public void testThreeItems() {
		cart.addItem(new ShopItem("Teddy Bear", 23.0));
		cart.addItem(new ShopItem("Teddy Bear", 23.0));
		cart.addItem(new ShopItem("Teddy Bear", 23.0));		
		cart.addItem(new ShopItem("Teddy Bear", 23.0));
		cart.addItem(new ShopItem("Teddy Bear", 23.0));		
		assertEquals(cart.getPrice(), 69.0, 0.001);
	}
	
	public void testRemovals() {
		cart.addItem(new ShopItem("Teddy Bear", 23.0));
		cart.addItem(new ShopItem("Teddy Bear", 23.0));
		cart.addItem(new ShopItem("Teddy Bear", 23.0));		
		cart.addItem(new ShopItem("Teddy Bear", 23.0));
		cart.addItem(new ShopItem("Teddy Bear", 23.0));		
		cart.removeItem(new ShopItem("Teddy Bear", 23.0));
		cart.removeItem(new ShopItem("Teddy Bear", 23.0));		
		assertEquals(cart.getPrice(), 46.0, 0.001);
	}
	
	

}
