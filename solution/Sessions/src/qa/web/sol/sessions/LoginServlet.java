package qa.web.sol.sessions;

import java.io.IOException;
import java.text.DecimalFormat;

import javax.servlet.ServletException;
import javax.servlet.http.*;

/**
 * @version 	1.0
 * @author lewis
 */
public class LoginServlet extends HttpServlet {

	private static final DecimalFormat nf = new DecimalFormat("00000");

	/**
	* @see javax.servlet.http.HttpServlet#void (javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	*/
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {

		String username = req.getParameter("username");
		String password = req.getParameter("password");
		if (!checkLogin(username, password)) {
			resp.sendRedirect("login");
		} else {
			req.getSession(true).setAttribute("username", username);
			resp.sendRedirect("loggedin");
		}

	}

	/**
	* @see javax.servlet.http.HttpServlet#void (javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	*/
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
			doGet(req, resp);
	}

	private boolean checkLogin(String username, String password) {
		boolean login = false;
		if (username == null || password == null) return false;
		int hash = Math.abs(username.hashCode() % 100000);
		String hashString = nf.format(hash);
		System.out.println("Password ought to be: " + hashString);
		return password.equals(hashString);
	}

}
